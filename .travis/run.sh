#!/bin/bash
if [ "$TRAVIS_OS_NAME" == "osx" ]; then
  ansible --version
  molecule test
fi
