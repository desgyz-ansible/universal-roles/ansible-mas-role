#!/bin/bash
if [ "$TRAVIS_OS_NAME" == "osx" ]; then
  if test -d "$HOME/.pyenv/versions/$PYTHON/bin" 
  then
    brew uninstall --ignore-dependencies openssl
    brew uninstall --ignore-dependencies readline
    brew uninstall --ignore-dependencies sqlite3
    export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
    python --version
  else
    brew update
    brew upgrade pyenv
    brew uninstall --ignore-dependencies openssl
    brew uninstall --ignore-dependencies readline
    brew uninstall --ignore-dependencies sqlite3
    pyenv install $PYTHON
    export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
    python --version
  fi
  if test -d "$HOME/.rvm/rubies/ruby-$RUBY/bin"
  then
    source $HOME/.rvm/scripts/rvm
    rvm fix-permissions
    rvm uninstall 2.4.2
    rvm uninstall 2.3.5
    export PATH="$HOME/.rvm/rubies/ruby-$RUBY/bin"
    ruby -v
  else
    source $HOME/.rvm/scripts/rvm
    rvm fix-permissions
    rvm get stable
    rvm reload
    rvm uninstall 2.4.2
    rvm uninstall 2.3.5
    source $HOME/.rvm/scripts/rvm
    curl -sSL http://pyyaml.org/download/libyaml/yaml-0.2.1.tar.gz -o yaml-0.2.1.tar.gz
    mv yaml-0.2.1.tar.gz $HOME/.rvm/archives/
    rvm pkg remove
    rvm autolibs rvm_pkg
    rvm pkg install autoconf
    rvm pkg install pkgconfig
    rvm pkg install readline
    rvm pkg install zlib
    rvm pkg install openssl
    rvm pkg install curl
    rvm pkg install gettext
    rvm pkg install libyaml
    rvm reload
    rvm requirements
    rvm install $RUBY --without-gmp
    rvm --default use $RUBY
    rvm fix-permissions
    export PATH="$HOME/.rvm/rubies/ruby-$RUBY/bin"
    ruby -v
  fi
fi
