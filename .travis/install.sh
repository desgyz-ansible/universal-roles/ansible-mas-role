#!/bin/bash
if test -d "$HOME/.pyenv/versions/$PYTHON/lib/python$PY/site-packages/ansible"
then
  export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
  python -m pip install ansible==$ANSIBLE
else
  if [[ "$PYTHON" == "2.7*" ]]; then
    export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python get-pip.py 
    python -m pip install ansible==$ANSIBLE semver molecule
  else
    export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
    python -m pip install -U pip
    python -m pip install ansible==$ANSIBLE semver molecule
  fi
fi
