import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_mas_dir(host):
    a = host.file('/usr/local/bin/mas')
    assert a.exists
    assert a.is_directory
    assert a.user == 'root'
    assert a.group == 'admin'
    assert a.mode == 0o755


def test_mas_is_useable(host):
    b = host.command('/usr/local/bin/mas version')
    assert b.rc == 0


def test_installed_packages(host):
    c = host.file('/Applications/Blackmagic Disk Speed Test (3.0)')
    assert c.exists

def test_softwareupdater_frequency_settings(host):
    d = host.command('/usr/libexec/PlistBuddy -c "Print :ScheduleFrequency" /User/travis/Library/Preferences/com.apple.appstore.plist')
    assert d.rc == 0


def test_softwareupdater_debug_settings(host):
    d = host.command('/usr/libexec/PlistBuddy -c "Print :ShowDebugMenu" /User/travis/Library/Preferences/com.apple.appstore.plist')
    assert d.rc == 0


def test_softwareupdater_webkit_settings(host):
    d = host.command('/usr/libexec/PlistBuddy -c "Print :WebKitDeveloperExtras" /User/travis/Library/Preferences/com.apple.appstore.plist')
    assert d.rc == 0